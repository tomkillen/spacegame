﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GazeFishEye : MonoBehaviour 
{
	public GameObject[] fishEyes;
	public GazeTracker tracker;

	public float smoothGainTime = 3f;
	public float smoothLoseTime = 1f;

	public float minFishEyeX = 0f;
	public float maxFishEyeX = 1f;
	public float minFishEyeY = 0f;
	public float maxFishEyeY = 1f;

	float smoothVelX = 0f;
	float fishX = 0f;
	float smoothVelY = 0f;
	float fishY = 0f;

	void Update()
	{
		if (tracker.hasTarget)
		{
			fishX = Mathf.SmoothDamp(fishX, maxFishEyeX, ref smoothVelX, smoothGainTime);
			fishY = Mathf.SmoothDamp(fishY, maxFishEyeY, ref smoothVelY, smoothGainTime);
		}
		else
		{
			fishX = Mathf.SmoothDamp(fishX, minFishEyeX, ref smoothVelX, smoothLoseTime);
			fishY = Mathf.SmoothDamp(fishY, minFishEyeY, ref smoothVelY, smoothLoseTime);
		}

		foreach (var fishEye in fishEyes)
		{
			fishEye.SendMessage("SetFishEyeX", fishX);
			fishEye.SendMessage("SetFishEyeY", fishY);
		}


	}
}
