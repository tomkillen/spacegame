﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class MoveInGazeDirection : MonoBehaviour 
{
	public float maxMoveSpeed = 100f;
	public float moveAccel = 20f;
	public float moveDecel = 100f;

	float moveSpeed = 0f;
	float accel = 0f;

	float accelV = 0f;

	GazeTracker tracker;

	public float minAberation = 0f;
	public float maxAberation = 25f;


	void Start()
	{
		tracker = FindObjectOfType<GazeTracker>();
	}

	void Update()
	{
		if (tracker.hasTarget)
		{
			accel = Mathf.SmoothDamp(accel, moveAccel, ref accelV, 1f);

			//this.transform.position = Vector3.MoveTowards(this.transform.position, tracker.currTarget.transform.position, moveSpeed * Time.de
			                                              
		}
		else
		{
			accel = Mathf.SmoothDamp(accel, moveDecel, ref accelV, 0.5f);
		}

		moveSpeed = Mathf.MoveTowards(moveSpeed, (tracker.hasTarget) ? maxMoveSpeed : 0f, accel * Time.deltaTime);
		this.transform.position = this.transform.position + this.transform.forward * moveSpeed;

		float speed01 = moveSpeed / maxMoveSpeed;
		this.gameObject.SendMessage("SetAberation", Mathf.Lerp(minAberation, maxAberation, speed01));

		this.rigidbody.velocity = Vector3.MoveTowards(this.rigidbody.velocity, Vector3.zero, 500f * Time.deltaTime);
	}
}
