﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GazeTracker : MonoBehaviour 
{
	public float distance = 10000f;
	public bool hasTarget = false;
	public GameObject currTarget;
	public float castRadius = 10f;

	void Update()
	{
		RaycastHit hit;


		hasTarget = Physics.SphereCast(new Ray(this.transform.position, this.transform.forward), castRadius, out hit);

		if (hasTarget)
		{
			currTarget = hit.collider.gameObject;
		}
		else
		{
			currTarget = null;
		}
	}
}
