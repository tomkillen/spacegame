﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Rotater : MonoBehaviour 
{
	public float rotSpeed = 1f;
	public Vector3 axis = new Vector3(0f, 1f, 0f);

	void Update()
	{
		this.transform.Rotate(axis, rotSpeed * Time.deltaTime);
	}
}
